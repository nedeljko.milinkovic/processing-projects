class Molecule{
   PVector radiusVector;
   public Molecule(PVector R){
      radiusVector = R;
   }
}
class Container{
float width;
float height;
float temperature;
float volume;
float pressure;
ArrayList<Molecule> molecules;
PVector pVec;


public Container(float w, float h, float t, float v, float p){
 width = w;
 height = h;
 temperature = t;
 volume = v;
 pressure = p; 
}

public void setPosition(float x,float y){
   pVec = new PVector(x,y);
}


public void initalizeMolecules(int numOfParticles){
   molecules = new ArrayList();
   for(int i=0;i<numOfParticles;++i){
      molecules.add(new Molecule(new PVector(random(0, this.width), random(0,this.height))));
   }
}

public void update(){

}

public void draw(){
   for(int i=0;i<molecules.size();++i){
       Molecule  m = molecules.get(i);
       fill(#FF0000);
       ellipse(this.pVec.x + m.radiusVector.x, this.pVec.y+ m.radiusVector.y, 10,10);
   }
}
}


Container mainContainer;


void setup() {
  
  mainContainer = new Container(300,300,300, 3, 101325);
  mainContainer.setPosition((width - mainContainer.width)/2,(height - mainContainer.height)/2); 
  mainContainer.initalizeMolecules(10);
  
  size(700, 680);
  background(#EEFCCF);
  fill(#FFFFFF);
  rect(mainContainer.pVec.x, mainContainer.pVec.y, mainContainer.width, mainContainer.height);  
  
  frameRate(1);
  noStroke();
}