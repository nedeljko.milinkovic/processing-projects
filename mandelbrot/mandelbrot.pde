
PVector[][] data;

void setup() {
  int[] col = waveLengthToRGB(wv);
  
  background(color(col[0],col[1],col[2]));
  size(1600,900);
  data = new PVector[width][];
  for(int i=0;i<width;++i){
     data[i] = new PVector[height];
  for(int j=0;j<height;++j){
     data[i][j] = new PVector(0,0);
  }
}
     
    

  noStroke();
}

int count = 0;
int time = 0;

float r=100,g=0,b=0;


int wv = 390;
float distance = sqrt(10);
void draw() {
  if(wv == 10)
    return;
  wv = (wv+10)%720;
 
  int[] col = waveLengthToRGB(wv);
 
  color cc =  color(col[0],col[1],col[2]);
  for(int i =-width/2; i <width/2; ++i){
     for(int j =-height/2; j <height/2; ++j){ 
        PVector c = new PVector(i/200.0f,j/200.0f);
        PVector prev =  data[i+width/2][j+height/2];
        float reNex = prev.x*prev.x - prev.y*prev.y + c.x;
          
                
                
        data[i+width/2][j+height/2].y = 2*prev.x*prev.y + c.y;
        data[i+width/2][j+height/2].x = reNex;
        
       
        
        if (DSQ(data[i+width/2][j+height/2], new PVector(0,0))<=distance*distance){           
             fill(cc);
             ellipse(  i+width/2,j+height/2, 1,1);
        }else{
        fill(#FFFFFF);
        }
        
       
     }
  }
    //<>//
  
  }
float DSQ(PVector A, PVector B) {
  float dx = A.x - B.x;
  float dy = A.y - B.y;
  return dx*dx + dy*dy;
}


static private double Gamma = 0.80;
static private double IntensityMax = 255;

public  int[] waveLengthToRGB(double Wavelength){
    double factor;
    double Red,Green,Blue;

    if((Wavelength >= 380) && (Wavelength<440)){
        Red = -(Wavelength - 440) / (440 - 380);
        Green = 0.0;
        Blue = 1.0;
    }else if((Wavelength >= 440) && (Wavelength<490)){
        Red = 0.0;
        Green = (Wavelength - 440) / (490 - 440);
        Blue = 1.0;
    }else if((Wavelength >= 490) && (Wavelength<510)){
        Red = 0.0;
        Green = 1.0;
        Blue = -(Wavelength - 510) / (510 - 490);
    }else if((Wavelength >= 510) && (Wavelength<580)){
        Red = (Wavelength - 510) / (580 - 510);
        Green = 1.0;
        Blue = 0.0;
    }else if((Wavelength >= 580) && (Wavelength<645)){
        Red = 1.0;
        Green = -(Wavelength - 645) / (645 - 580);
        Blue = 0.0;
    }else if((Wavelength >= 645) && (Wavelength<781)){
        Red = 1.0;
        Green = 0.0;
        Blue = 0.0;
    }else{
        Red = 0.0;
        Green = 0.0;
        Blue = 0.0;
    };

    // Let the intensity fall off near the vision limits

    if((Wavelength >= 380) && (Wavelength<420)){
        factor = 0.3 + 0.7*(Wavelength - 380) / (420 - 380);
    }else if((Wavelength >= 420) && (Wavelength<701)){
        factor = 1.0;
    }else if((Wavelength >= 701) && (Wavelength<781)){
        factor = 0.3 + 0.7*(780 - Wavelength) / (780 - 700);
    }else{
        factor = 0.0;
    };


    int[] rgb = new int[3];

    // Don't want 0^x = 1 for x <> 0
    rgb[0] = Red==0.0 ? 0 : (int) Math.round(IntensityMax * Math.pow(Red * factor, Gamma));
    rgb[1] = Green==0.0 ? 0 : (int) Math.round(IntensityMax * Math.pow(Green * factor, Gamma));
    rgb[2] = Blue==0.0 ? 0 : (int) Math.round(IntensityMax * Math.pow(Blue * factor, Gamma));

    return rgb;
}