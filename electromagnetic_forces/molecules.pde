class Molecule {
  PVector R;
  float r;
  boolean aggregated;
  PVector magField;
  int heat;
  float charge=-1.0f;
  PVector velocity;
  double vMag = 13.0;
  PVector coulombForce;
  Molecule() {
    R = new PVector(random(0, width), random(0, height));
     
    float xxx = random(0,100);
    if(xxx>50.0f){
      charge = 1.0f;
    }
    
    
    r = 10.5;
    prevPos = new PVector(0,0);
    aggregated = false;
    heat = 0;
    magField = new PVector(0,0,0.09);
    
    //velocity = new PVector(2,0,0);
    velocity = new PVector(random(0,3), random(0,3));
    
  }
  PVector prevPos;
  void moveMe() {
      prevPos.x = R.x;
      prevPos.y = R.y;
      
      R.add(velocity);
 
      if(R.x>350){
        PVector force =  velocity.cross(magField);
        force.x *=charge;
        force.y *=charge;
        //force.normalize();
        velocity.add(force);
        velocity.normalize();
        velocity.x *=vMag;
        velocity.y *=vMag;
      }
      
      coulombForce = new PVector(0,0);
      
      for (int i = 0; i < molecules.size(); i++) {
        if(molecules.get(i) == this)
            continue;
            
          PVector dist = new PVector(molecules.get(i).R.x - R.x,molecules.get(i).R.y - R.y);
          float d = (DSQ(molecules.get(i).R, R));
          if(d<=4*r*r)
            d = 4*r*r;
          dist.normalize();
          dist.x *= 1/d;
          dist.y *= 1/d;
          
          if(molecules.get(i).charge == charge){
             dist.x *= -1;
             dist.y *= -1;
          }
          
          
          coulombForce.add(dist);
      }
      coulombForce.x *=1000.0;
      coulombForce.y *=1000.0;
      
      println(coulombForce.x);
      
      velocity.add(coulombForce);
      
      if (R.x < 0) {
        velocity.x *=-1;
      }
      if (R.x > width) {
        velocity.x *=-1;
      }
      if (R.y < 0) {
        velocity.y *=-1;
      }
      if (R.y > height) {
        velocity.y *=-1;
  }
  }
  void drawMe() {
    if(charge==1.0f)
      fill(#2946A1);
    else 
      fill(#A14629);
    
     ellipse(R.x,R.y,r,r);
 
  }
  void increaseHeat(){
    heat++;
  }
}


void setup() {
  size(700, 680);
  background(#EEFCCF);
  
  for (int i = 0; i < 100; i++) {
    molecules.add(new Molecule());
  }
  //frameRate(32);
  noStroke();
  Molecule kek = molecules.get(0);
  kek.R = new PVector(width/2, height);
  kek.aggregated = true;
}